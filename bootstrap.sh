virtualenv env
source env/bin/activate
git clone https://cinquemb@bitbucket.org/cinquemb/django_file_api.git django_file_api
mkdir django_file_api/file_http_api/file_http_api_uploads
pip install -r django_file_api/requirements.txt
python django_file_api/file_http_api/manage.py runserver [::]:8000
