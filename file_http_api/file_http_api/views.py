from django.http import HttpResponse, HttpResponseBadRequest, FileResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from file_http_api import settings
from .forms import UploadFileForm, RetriveFileForm, DeleteFileForm
import json
import codecs


@csrf_exempt
def upload_file(request):
	#checking if request is post request
    if request.method == 'POST':
    	file_data_info = None
    	file_data = None
    	#grab title from data and file and pass into form while formatting form object according definition in forms.py
    	if len(request.FILES) == 1:
    		if request.FILES.keys() > 0:
    			file_data_info = {
    				'title': request.FILES.keys()[0],
    				'file_': request.FILES.values()[0]
    			}
    			file_data = {
    				'file_': request.FILES.values()[0]
    			}
    		else:
    			return HttpResponseBadRequest('invalid file upload\n', content_type="text/plain")
    	else:
    		return HttpResponseBadRequest('invalid file upload\n', content_type="text/plain")

        form = UploadFileForm(file_data_info, file_data)
        #pefeforms form validation
        if form.is_valid():
        	#saving file and returning status
        	form.save()
        	return HttpResponse('file successfully uploaded\n', content_type="text/plain")
       	else:
       		#dumping errors as json
       		return HttpResponseBadRequest(json.dumps(form.errors) + '\n', content_type="text/json")
    else:
    	return HttpResponseBadRequest('invalid file upload\n', content_type="text/plain")

@csrf_exempt
def retrieve_file(request):
	#checking if file is get request
	if request.method == 'GET':
		form = RetriveFileForm(request.GET, request.FILES)
		#pefeforms form validation
		if form.is_valid():
			#returning the data from the file for the given query
			return FileResponse(codecs.open(settings.BASE_DIR + settings.MEDIA_URL +request.GET['title']))
		else:
			#dumping errors as json
			return HttpResponseBadRequest(json.dumps(form.errors) + '\n', content_type="text/plain")
	else:
		return HttpResponseBadRequest('invalid file upload\n', content_type="text/plain")

@csrf_exempt
def delete_file(request):
	#checking if file is get request
	if request.method == 'GET':
		form = DeleteFileForm(request.GET, request.FILES)
		#pefeforms form validation
		if form.is_valid():
			#saving file and returning status
			form.save()
			return HttpResponse('file successfully deleted\n', content_type="text/plain")
		else:
			#dumping errors as json
			return HttpResponseBadRequest(json.dumps(form.errors) + '\n', content_type="text/plain")
	else:
		return HttpResponseBadRequest('invalid file deltion\n', content_type="text/plain")
