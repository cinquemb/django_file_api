from django import forms
from django.conf import settings
import codecs
import os

class UploadFileForm(forms.Form):
	#title of file
    title = forms.CharField(max_length=100)
    #file data
    file_ = forms.FileField()

    def clean(self):
    	super(UploadFileForm, self).clean()
        #check if file name is greater than limit
        if len(self.cleaned_data['title']) > 100:
            raise forms.ValidationError("invalid file name")
        #checking if any weird characters are in file
        if any(x in self.cleaned_data['title'] for x in [';','*',':']):
        	raise forms.ValidationError("invalid file name")
        #check if file exits
        if os.path.isfile(settings.BASE_DIR + settings.MEDIA_URL + self.cleaned_data['title']):
        	raise forms.ValidationError("file exists")
        return self.cleaned_data

    def save(self):
    	#saving file from media directory
    	with codecs.open(settings.BASE_DIR + settings.MEDIA_URL + self.cleaned_data['title'], 'wb+', 'utf8') as destination:
	        for chunk in self.cleaned_data['file_'].chunks():
	            destination.write(chunk)

class RetriveFileForm(forms.Form):
    title = forms.CharField(max_length=100)
    def clean(self):
        super(RetriveFileForm, self).clean()
        #check if file name is greater than limit
        if len(self.cleaned_data['title']) > 100:
            raise forms.ValidationError("invalid file name")
        #checking if any weird characters are in file
        if any(x in self.cleaned_data['title'] for x in [';','*',':']):
        	raise forms.ValidationError("invalid file name")
        #check if file does not exist
        if not os.path.isfile(settings.BASE_DIR + settings.MEDIA_URL + self.cleaned_data['title']):
        	raise forms.ValidationError("invalid file name")
        return self.cleaned_data

class DeleteFileForm(forms.Form):
    title = forms.CharField(max_length=100)

    def clean(self):
        super(DeleteFileForm, self).clean()
        #check if file name is greater than limit
        if len(self.cleaned_data['title']) > 100:
            raise forms.ValidationError("invalid file name")
        #checking if any weird characters are in file
        if any(x in self.cleaned_data['title'] for x in [';','*',':']):
        	raise forms.ValidationError("invalid file name")
        #check if file exist
        if not os.path.isfile(settings.BASE_DIR + settings.MEDIA_URL + self.cleaned_data['title']):
        	raise forms.ValidationError("invalid file name")
        return self.cleaned_data

    def save(self):
    	#deleting the file when save is called from media directory
    	os.remove(settings.BASE_DIR + settings.MEDIA_URL + self.cleaned_data['title'])
    	return 'file deleted'

